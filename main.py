import re

REGEX_GENERIC_KEYVAL = r'\"(.*)\" \"(.*)\"'     # Matches a "key" "val" string and makes capture groups. Group 0 is the key, group 1 is the value.

MOM_ENT_KEYS = ("zone_type",
                "spawnflags",
                "speed_limit",
                "speed_limit_type",
                "start_on_jump",
                "track_number",
                "zone_number",
                "solids"
                )


class LolThisIsAnException(Exception):
    pass


class ReadVMF:
    def __init__(self, vmf_text):
        self.vmf_text = vmf_text

        # Get the file's lines into a list and strip whitespace
        self.vmf_lines = []
        for line in self.vmf_text.split('\n'):
            self.vmf_lines.append(line.strip())

        self.ENTS = []
        IN_ENTITY = False
        IN_SOLID = False
        IN_SIDE = False
        IN_POINT_DATA = False
        IN_EDITOR = False


        for linenum, line in enumerate(self.vmf_lines):

            # When we find an entity, set IN_ENTITY to True.
            if line == 'entity' and not IN_ENTITY:
                IN_ENTITY = True
                print("Found an entity... Line %d" % linenum)
                continue

            # When we find an entity's classname...
            z = re.match(r'\"classname\" \"(.*)_(.*)\"', line)
            if z and IN_ENTITY:
                # If it's not a Momentum timer trigger, ignore it.
                if z.groups()[0] != 'trigger_momentum_timer':
                    IN_ENTITY = False
                    print("...but not a Momentum timer trigger. Skipping.")
                    pass
                # If it is, make a dict with the keys for this entity.
                else:
                    print("...and it's a Momentum timer trigger!")
                    self.MOM_ENT = {key: None for key in MOM_ENT_KEYS}
                    self.MOM_ENT['solids'] = []
                    # Set the zone type according to what kind it is.
                    if z.groups()[1] == 'stop':
                        self.MOM_ENT['zone_type'] = 0
                    elif z.groups()[1] == 'start':
                        self.MOM_ENT['zone_type'] = 1
                    elif z.groups()[1] == 'stage':
                        self.MOM_ENT['zone_type'] = 2
                    elif z.groups()[1] == 'checkpoint':
                        self.MOM_ENT['zone_type'] = 3
                    else:
                        raise LolThisIsAnException("what the fuck kind of zone is this?")
                continue

            # If we find the start of a side, set IN_SIDE to True.
            if line == 'side' and IN_ENTITY and IN_SOLID:
                IN_SIDE = True
#                print("Found a side! Line %d" % linenum)
                continue

            # If we find the start of a solid, set IN_SOLID to True and make a list where we'll put its sides.
            if line == 'solid' and IN_ENTITY and not IN_SOLID:
                IN_SOLID = True
                self.SOLID = []
                print("Found a solid! Line %d" % linenum)
                continue

            # If we find the start of a "editor" block, set IN_EDITOR to True.
            if line == 'editor' and IN_ENTITY:
                IN_EDITOR = True
                continue

            # If we find the start of some pointdata, set IN_POINT_DATA to True and make a list for it.
            if line == 'point_data' and IN_SIDE and not IN_POINT_DATA:
                IN_POINT_DATA = True
                self.SOLID_POINTDATA = []
                continue

            # If we find a point keyval in a point_data block, add it to self.SOLID_POINTDATA.
            # The capture groups in this regex match the X, Y and Z coordinates of the point.
            z = re.match(r'\"point\" \"\d* (-?\d*\.?\d*) (-?\d*\.?\d*) (-?\d*\.?\d*)\"', line)
            if z and IN_POINT_DATA:
                # Convert strings in the capture groups to floats
                self.SOLID_POINTDATA.append([float(coord) for coord in z.groups()])
#                print("Found point data! Line %d" % linenum)
                continue

            # Look for any keyval
            z = re.match(REGEX_GENERIC_KEYVAL, line)
            if z:
                # If this line is a keyval, and we're in an entity (but not yet in a solid or side)...
                if IN_ENTITY and not IN_SOLID and not IN_SIDE and not IN_POINT_DATA and not IN_EDITOR:
                    # If this key is in our dictionary, add its value to the dict.
                    if z.groups()[0] in MOM_ENT_KEYS:
                        # Convert the strings to either an int or a float depending on whether it's a decimal.
                        try:
                            self.MOM_ENT[z.groups()[0]] = int(z.groups()[1])
                        except ValueError:
                            self.MOM_ENT[z.groups()[0]] = float(z.groups()[1])
                    continue


            # When we find the end of an {editor} block, set IN_EDITOR to False.
            if line == '}' and IN_EDITOR:
#                print("Found the end of an \"editor\" block! Line %d" % linenum)
                IN_EDITOR = False
                continue

            # When we find the end of a {point_data} block, set IN_POINTDATA to False
            # and append the pointdata to the solid's list.
            if line == '}' and IN_ENTITY and IN_SOLID and IN_SIDE and IN_POINT_DATA and not IN_EDITOR:
#                print("Found the end of a point_data block! Line %d" % linenum)
                IN_POINT_DATA = False
                self.SOLID.append(self.SOLID_POINTDATA)
                continue

            # When we find the end of a side, set IN_SIDE to False.
            if line == '}' and IN_ENTITY and IN_SOLID and IN_SIDE and not IN_POINT_DATA and not IN_EDITOR:
#                print("Found the end of a side! Line %d" % linenum)
                IN_SIDE = False
                continue

            # When we find the end of a solid, set IN_SOLID to False and append it to the MOM_ENT's solids list.
            if line == '}' and IN_ENTITY and IN_SOLID and not IN_SIDE and not IN_POINT_DATA and not IN_EDITOR:
#                print("Found the end of a solid! Line %d" % linenum)
                IN_SOLID = False
                self.MOM_ENT['solids'].append(self.SOLID)
                continue

            # When we find the end of an entity, set IN_ENTITY to False and append the entity to the entity list.
            if line == '}' and IN_ENTITY and not IN_SOLID and not IN_SIDE and not IN_POINT_DATA and not IN_EDITOR:
                print("Found the end of an entity! Line %d" % linenum)
                IN_ENTITY = False
                self.ENTS.append(self.MOM_ENT)
                continue

    def get_parsed_vmf(self):
        return self.ENTS


class WriteZon:
    def __init__(self, parsed_vmf):
        self.parsed_vmf = sorted(parsed_vmf, key=lambda x: (x['track_number'], ['zone_number']))   # Sort by track number and zone number.
        self.zon = '\"tracks\"\n{\n'

        trackNum = -1
        for zone in self.parsed_vmf:
            # If we've reached a new track, add a new track to the zon.
            if zone['track_number'] > trackNum:
                trackNum = zone['track_number']
                # Add a closing bracket for the previous track (if it's not the first track).
                if trackNum != 0:
                    self.zon += "	}\n"
                self.zon += '	\"{trackNum}\"\n	{{\n'.format(trackNum = zone['track_number'])
            # If we're still on the same track, just add the zone without making a new track.
            else:
                pass

            # Add the zone to the zon file
            self.zon += """		\"{zoneNum}\"
		{{
			\"zoneNum\"		\"{zoneNum}\"
            \"triggers\"
            {{\n""".format(zoneNum = zone['zone_number'])

            # Add the zone's triggers
            for triggerNum, solid in enumerate(zone['solids']):
                self.zon += '				\"{triggerNum}\"\n                {{\n'.format(triggerNum = triggerNum)

                # If it's a startzone (zone_type 1), add the zoneProps block.
                # Eventually, stage zones will have this too.
                if zone['zone_type'] == 1:
                    # Check spawnflags to see if "Limit leave speed" is checked.
                    if (zone['spawnflags'] & 8192):
                        limiting_speed = 1
                    else:
                        limiting_speed = 0

                    self.zon += """    				"zoneProps"
    				{{
    					"properties"
    					{{
    						"speed_limit"		"{speed_limit}"
    						"limiting_speed"		"{limiting_speed}"
    						"start_on_jump"		"{start_on_jump}"
    						"speed_limit_type"		"{speed_limit_type}"
    					}}
    				}}""".format(speed_limit = zone['speed_limit'],
                                 limiting_speed = limiting_speed,
                                 start_on_jump = zone['start_on_jump'],
                                 speed_limit_type = zone['speed_limit_type'])
                # If it's not a startzone, don't add zoneProps.
                else:
                    pass

                # Add the zone's properties (not to be confused with zoneProps).
                pointsZPos = self.get_pointsZPos(solid)
                pointsHeight = self.get_pointsHeight(solid)
                self.zon += """
					"zoneNum"		"{zoneNum}"
					"type"		"{zoneType}"
					"pointsZPos"		"{pointsZPos}"
					"pointsHeight"		"{pointsHeight}"
					"points"
					{{
""".format(zoneNum = zone['zone_number'],
           zoneType = zone['zone_type'],
           pointsZPos = pointsZPos,
           pointsHeight = pointsHeight)

                # Add the trigger's points
                points = []
                for side in solid:
                    for point in side:
                        # Don't add duplicate points
                        if ([point[0], point[1]]) not in points:
                            points.append([point[0], point[1]])     # Only put in the X and Y

                for pointnum, point in enumerate(points):
                    self.zon += '						\"p{pointnum}\"		\"{pointX} {pointY}\"\n'.format(pointnum = pointnum,
                                                                                                          pointX = point[0],
                                                                                                          pointY = point[1])
                # Once done adding the points, add closing brackets.
                self.zon += "					}\n				}\n"

            # Once done adding all of this zone's triggers, add closing brackets.
            self.zon += "			}\n		}\n"

        # Once done adding everything, add closing brackets.
        self.zon += "	}\n}"






        with open('output.zon', 'w') as output:
            output.write(self.zon)



    def get_pointsZPos(self, solid):
        """Pass this a solid array and it returns its pointsZPos,
        which is the lowest Z coordinate out of all the points."""
        return min([point for side in solid for point in side], key=lambda x: x[2])[2]


    def get_pointsHeight(self, solid):
        """Pass this a solid array and it returns its pointsHeight,
        which is the difference between the highest and lowest Z coordinates
        of all the points."""
        lowZ = self.get_pointsZPos(solid)
        highZ = max([point for side in solid for point in side], key=lambda x: x[2])[2]
        return highZ - lowZ





if __name__ == '__main__':
    with open('testmap.vmf', 'r') as file:
        inst = ReadVMF(file.read())
        zones = inst.get_parsed_vmf()
        memes = WriteZon(zones)
